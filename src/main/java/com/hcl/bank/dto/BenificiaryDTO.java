package com.hcl.bank.dto;

import javax.validation.constraints.NotNull;

public class BenificiaryDTO {

	@NotNull(message = "account number required")
	private Long benificiaryAccountNumber;

	@NotNull(message = "benificiary name is required")
	private String benificiaryName;

	@NotNull(message = "customer id is required")
	private Long customerId;

	public Long getBenificiaryAccountNumber() {
		return benificiaryAccountNumber;
	}

	public void setBenificiaryAccountNumber(Long benificiaryAccountNumber) {
		this.benificiaryAccountNumber = benificiaryAccountNumber;
	}

	public String getBenificiaryName() {
		return benificiaryName;
	}

	public void setBenificiaryName(String benificiaryName) {
		this.benificiaryName = benificiaryName;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

}
