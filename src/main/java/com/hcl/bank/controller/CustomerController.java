package com.hcl.bank.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.bank.response.CustomerResponse;
import com.hcl.bank.service.CustomerService;

@RestController
@RequestMapping("/customers")
public class CustomerController {

	private Logger logger = LoggerFactory.getLogger(CustomerController.class);

	@Autowired
	private CustomerService customerService;
	

	/**
	 *  customer login
	 * @param emailId
	 * @param password
	 * @return login successful message with status code
	 */
	@PostMapping("")
	public ResponseEntity<CustomerResponse> customerLogin(@RequestParam(required = true) String emailId,
			@RequestParam(required = true) String password) {

		logger.info("user login");

		CustomerResponse customerResponse = customerService.customerLogin(emailId.trim(), password.trim());

		return new ResponseEntity<>(customerResponse, HttpStatus.OK);
	}

	/**
	 *  customer logout
	 * @param emailId 
	 * @return logout successful message with status code 
	 *     
	 */
	@PutMapping("")
	public ResponseEntity<CustomerResponse> customerLogout(@RequestParam(required = true) String emailId) {

		logger.info("user logout");

		CustomerResponse customerResponse = customerService.customerLogout(emailId.trim());

		return new ResponseEntity<>(customerResponse, HttpStatus.OK);
	}

}
