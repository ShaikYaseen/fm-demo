package com.hcl.bank.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "account")
public class Account {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "account_id")
	private Long accountId;

	@Column(name = "account_number", unique = true)
	private Long accountNumber;

	@Column(name = "account_type")
	private String accountType;

	@Column(name = "available_balance")
	private Double availableBalance;

	@Column(name = "ifsc_code")
	private String ifscCode;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Customer customer;

	@OneToMany(mappedBy = "accounts", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Transaction> transactions = new ArrayList<Transaction>();
	
	

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public void setAccountNumber(Long accountNumber) {
		this.accountNumber = accountNumber;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public void setAvailableBalance(Double availableBalance) {
		this.availableBalance = availableBalance;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public void setTransactions(List<Transaction> transactions) {
		this.transactions = transactions;
	}

	public Long getAccountId() {
		return accountId;
	}

	public Long getAccountNumber() {
		return accountNumber;
	}

	public String getAccountType() {
		return accountType;
	}

	public Double getAvailableBalance() {
		return availableBalance;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public Customer getCustomer() {
		return customer;
	}

	public List<Transaction> getTransactions() {
		return transactions;
	}


}
