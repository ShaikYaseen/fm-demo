
package com.hcl.bank.service;

import org.springframework.stereotype.Service;

import com.hcl.bank.dto.BenificiaryDTO;
import com.hcl.bank.dto.BenificiaryRequest;
import com.hcl.bank.response.BenificiaryResponse;

@Service
public interface BenificiaryService {

	public BenificiaryResponse addBenificiary(BenificiaryRequest benificiaryRequest);

	public BenificiaryResponse updatedBenificiary(BenificiaryDTO benificiaryRequest);

	public BenificiaryResponse deleteBenificiary(Long benificiaryId);

	public BenificiaryDTO getBenificiaryByBenificiaryId(Long benificiaryId);

}
