package com.hcl.bank.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.bank.dto.BenificiaryDTO;
import com.hcl.bank.dto.BenificiaryRequest;
import com.hcl.bank.response.BenificiaryResponse;
import com.hcl.bank.response.CustomerResponse;
import com.hcl.bank.service.BenificiaryService;
import com.hcl.bank.service.CustomerService;

@RestController
@RequestMapping("/benificiaries")
public class BenificiaryController {

	private Logger logger = LoggerFactory.getLogger(BenificiaryController.class);
	@Autowired
	CustomerService customerService;
	
	@Autowired
	BenificiaryService benificiaryService;

	/**
	 * @param benificiaryRequest
	 * @return
	 */
	@PostMapping("")
	public ResponseEntity<CustomerResponse> customerLogin(@RequestParam(required = true) String emailId,
			@RequestParam(required = true) String password) {

		logger.info("user login");

		CustomerResponse customerResponse = customerService.customerLogin(emailId.trim(), password.trim());

		return new ResponseEntity<>(customerResponse, HttpStatus.OK);
	}
	
	@PostMapping("")
	public ResponseEntity<BenificiaryResponse> addBenificiary(
			@Valid @RequestBody BenificiaryRequest benificiaryRequest) {

		logger.info("add benificiery");
        System.out.println("uday");
		BenificiaryResponse benificiaryResponse = benificiaryService.addBenificiary(benificiaryRequest);
		return new ResponseEntity<>(benificiaryResponse, HttpStatus.OK);

	}

	/**
	 * 
	 * @param benificiaryRequest
	 * @return updated successfully
	 */
	@PutMapping("")
	public ResponseEntity<BenificiaryResponse> updateBenificiary(@RequestBody BenificiaryDTO benificiaryRequest) {

		BenificiaryResponse benificiaryResponse = benificiaryService.updatedBenificiary(benificiaryRequest);
		return new ResponseEntity<>(benificiaryResponse, HttpStatus.OK);
	}

	/**
	 * 
	 * @param benificiaryId
	 * @return deleted successfully
	 */
	@DeleteMapping("/{benificiaryId}")
	public ResponseEntity<BenificiaryResponse> deleteBenificiary(@PathVariable("benificiaryId") Long benificiaryId) {
		BenificiaryResponse benificiaryResponse = benificiaryService.deleteBenificiary(benificiaryId);

		return new ResponseEntity<>(benificiaryResponse, HttpStatus.OK);
	}

	/**
	 * 
	 * @param benificiaryId
	 * @return beneficiaries with beneficiary id
	 */
	@GetMapping("/{benificiaryId}")
	public ResponseEntity<Object> getBenificiaryByBenificiaryId(@PathVariable("benificiaryId") Long benificiaryId) {
		BenificiaryDTO benificiaryDTO = benificiaryService.getBenificiaryByBenificiaryId(benificiaryId);
		return new ResponseEntity<>(benificiaryDTO, HttpStatus.OK);
	}
}
