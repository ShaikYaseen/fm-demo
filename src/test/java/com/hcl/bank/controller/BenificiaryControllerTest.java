package com.hcl.bank.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hcl.bank.dto.BenificiaryDTO;
import com.hcl.bank.dto.BenificiaryRequest;
import com.hcl.bank.entity.Account;
import com.hcl.bank.entity.Benificiary;
import com.hcl.bank.entity.Customer;
import com.hcl.bank.exception.AccountNotFoundException;
import com.hcl.bank.exception.BenificiaryNotFoundException;
import com.hcl.bank.exception.UserNotFoundException;
import com.hcl.bank.response.BenificiaryResponse;
import com.hcl.bank.service.BenificiaryService;

@SpringBootTest
public class BenificiaryControllerTest {

	@MockBean
	BenificiaryService benificiaryService;

	@Autowired
	BenificiaryController beneificaryController;

	Benificiary beneficiary = null;
	BenificiaryDTO benificiaryDTO = null;
	BenificiaryDTO benificiaryDTO2 = null;
	BenificiaryRequest benificiaryRequest = null;

	Account account = null;

	@Before
	public void add() {

		benificiaryRequest = new BenificiaryRequest();
		beneficiary = new Benificiary();

		Customer customer = new Customer();
		customer.setCustomerId(1000L);
		benificiaryRequest.setAccountNumber(1L);
		benificiaryRequest.setName("ram");
		benificiaryRequest.setCustomerId(4L);
		beneficiary.setBenificiaryId(1L);
		beneficiary.setBenificiaryAccountNumber(1000L);
		beneficiary.setBenificiaryName("vasu");
		beneficiary.setCustomer(customer);

		account = new Account();
		account.setAccountNumber(2000L);

		benificiaryDTO = new BenificiaryDTO();

	}

	@Test
	public void testAddBenificiary() {
		String expected = "Benificiary added successfully.";
		BenificiaryResponse benificiaryResponse = new BenificiaryResponse(expected, 777);
		Mockito.when(benificiaryService.addBenificiary(benificiaryRequest)).thenReturn(benificiaryResponse);
		ResponseEntity<BenificiaryResponse> response = beneificaryController.addBenificiary(benificiaryRequest);
		assertEquals(benificiaryResponse, response.getBody());

	}

	@Test
	public void testUpdateBenificiary()
			throws BenificiaryNotFoundException, UserNotFoundException, AccountNotFoundException {

		benificiaryDTO = new BenificiaryDTO();
		benificiaryDTO.setBenificiaryAccountNumber(1L);
		benificiaryDTO.setBenificiaryName("ram");
		benificiaryDTO.setCustomerId(1L);

		String expected = "Benificiary updated successfully.";
		BenificiaryResponse benificiaryResponse = new BenificiaryResponse(expected, 666);
		Mockito.when(benificiaryService.updatedBenificiary(benificiaryDTO)).thenReturn(benificiaryResponse);
		ResponseEntity<BenificiaryResponse> response = beneificaryController.updateBenificiary(benificiaryDTO);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
	public void testUpdateBenificiaryNegative() {

		benificiaryDTO = new BenificiaryDTO();
		benificiaryDTO.setBenificiaryAccountNumber(100L);
		benificiaryDTO.setBenificiaryName("ram");
		benificiaryDTO.setCustomerId(100L);

		String expected = "Benificiar Deleted Successfully";
		BenificiaryResponse benificiaryResponse = new BenificiaryResponse(expected, 666);
		Mockito.when(benificiaryService.updatedBenificiary(benificiaryDTO)).thenReturn(benificiaryResponse);
		ResponseEntity<BenificiaryResponse> response = beneificaryController.updateBenificiary(benificiaryDTO);
		assertEquals(benificiaryResponse, response.getBody());
	}

	@Test
	public void testDelete() {

		Long benificiaryId = 1L;
		String expected = "Benificiar Deleted Successfully";
		BenificiaryResponse benificiaryResponse = new BenificiaryResponse(expected, 888);
		Mockito.when(benificiaryService.deleteBenificiary(benificiaryId)).thenReturn(benificiaryResponse);
		ResponseEntity<BenificiaryResponse> response = beneificaryController.deleteBenificiary(benificiaryId);
		assertEquals(benificiaryResponse, response.getBody());
	}

	@Test
	public void testDeleteNegative() {

		Long benificiaryId = 2000L;
		String expected = "Benificiar Deleted Successfully";
		BenificiaryResponse benificiaryResponse = new BenificiaryResponse(expected, 888);
		Mockito.when(benificiaryService.deleteBenificiary(benificiaryId)).thenReturn(benificiaryResponse);
		ResponseEntity<BenificiaryResponse> response = beneificaryController.deleteBenificiary(benificiaryId);
		assertNotEquals(expected, response.getBody());
	}

	@Test
	public void testGetBenificiaryByBenificiaryIdPositive()  {

		benificiaryDTO = new BenificiaryDTO();
		benificiaryDTO.setBenificiaryAccountNumber(1L);
		benificiaryDTO.setBenificiaryName("ram");
		benificiaryDTO.setCustomerId(1L);

		Long benificiaryId = 1L;
		Mockito.when(benificiaryService.getBenificiaryByBenificiaryId(benificiaryId)).thenReturn(benificiaryDTO);
		ResponseEntity<Object> response = beneificaryController.getBenificiaryByBenificiaryId(benificiaryId);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}
	
	

}