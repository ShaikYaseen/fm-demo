package com.hcl.bank.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FundTransferResponse {

	@JsonProperty("message")
	private String message;
	
	@JsonProperty("statusCode")
	private Integer statusCode = 770;

	@JsonProperty("statusCode")
	public Integer getStatusCode() {
		return statusCode;
	}

	@JsonProperty("statusCode")
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public FundTransferResponse(String message, Integer statusCode) {
		super();
		this.message = message;
		this.statusCode = statusCode;
	}

	@JsonProperty("message")
	public String getMessage() {
		return message;
	}

	@JsonProperty("message")
	public void setMessage(String message) {
		this.message = message;
	}

}
