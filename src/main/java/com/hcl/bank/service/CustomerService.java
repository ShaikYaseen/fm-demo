package com.hcl.bank.service;

import org.springframework.stereotype.Service;

import com.hcl.bank.response.CustomerResponse;

@Service
public interface CustomerService {

	CustomerResponse customerLogin(String emailId, String password);

	CustomerResponse customerLogout(String emailId);

	public Boolean checkLoggingStatus(Long customerId);

}
